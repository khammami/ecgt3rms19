import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import java.util.Date;

@Entity(tableName = "word_table")
public class Word {

    @Ignore
    public Word(int id) {
        this.id = id;
    }

    public Word(String title, String content, Date published_on, @NonNull String mWord) {
        this.title = title;
        this.content = content;
        this.published_on = published_on;
        this.mWord = mWord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }

    @NonNull
    public String getmWord() {
        return mWord;
    }

    public void setmWord(@NonNull String mWord) {
        this.mWord = mWord;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String content;
    private Date published_on;
    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;
    public String getWord(){return this.mWord;}
}