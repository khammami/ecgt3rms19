package com.example.blogapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {post.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})

public abstract class postRoomDatabase  extends RoomDatabase {



    public abstract postDao postDao();
    private static postRoomDatabase  INSTANCE;

    static postRoomDatabase  getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (postRoomDatabase .class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            postRoomDatabase .class, "post_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }





}
