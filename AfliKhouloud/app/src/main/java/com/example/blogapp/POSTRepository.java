package com.example.blogapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class POSTRepository {

    private postDao mpostDao;

    private LiveData<List<post>> mAllposts;


    POSTRepository(Application application) {
        postRoomDatabase db = postRoomDatabase.getDatabase(application);
        mpostDao = db.postDao();
        mAllposts = mpostDao.getAllposts();
    }

    LiveData<List<post>> getmAllposts() {
        return mAllposts;
    }

    public void insert (post post) {
        new insertAsyncTask(mpostDao).execute(post);
    }
    public void delete (post post) {
        new deleteAsyncTask(mpostDao).execute(post);
    }
    public void deleteall () {
        new deleteAllUsersAsyncTask (mpostDao).execute();
    }



    private static class insertAsyncTask extends AsyncTask<post, Void, Void> {

        private postDao mAsyncTaskDao;

        insertAsyncTask(postDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final post... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}



