package com.example.blogapp;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "post_table")
public class post {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    private String title;
    private String content;
    private Date published_on;
    @Ignore

    public post(String title, String content, Date published_on) {
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }
}
