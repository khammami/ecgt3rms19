package com.example.blogapp;

import android.content.Intent;
import android.icu.text.CaseMap;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {

    private PostViewModel uv;
    adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            //
            uv = ViewModelProviders.of(this).get(PostViewModel.class);
            uv.getAllposts().observe(this, new Observer<List<post>>() {
                @Override
                public void onChanged(@Nullable final List<post> users) {
                    // Update the cached copy of the words in the adapter.
                    adapter.setUser(users);
                }
            });
            adapter = new adapter(this);
            RecyclerView rv = (RecyclerView) findViewById(R.id.rec);
            rv.setAdapter(adapter);
            rv.setLayoutManager(new LinearLayoutManager(this));

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, ajout.class);
                    startActivityForResult(i, 1);

                }
            });


            ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {


                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {


                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = viewHolder.getAdapterPosition();
                    post post = adapter.getitem(position);

                    uv.delete(post);
                    Toast.makeText(MainActivity.this, Integer.toString(direction), Toast.LENGTH_SHORT).show();

                }


            });
            helper.attachToRecyclerView(rv);
        }

        @Override
        protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == 1)
                if (resultCode == RESULT_OK) {
                    String Title = data.getStringExtra("Title");
                    String content = data.getStringExtra("content");
                    Date published_on = data.getDataString() ("DATE");

                    uv.insert(new post(Title, content, published_on));
                }

        }
    }


















    }

