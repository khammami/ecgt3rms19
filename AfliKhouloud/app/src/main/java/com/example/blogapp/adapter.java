package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class adapter  extends RecyclerView.Adapter<adapter.UserViewHolder> {





        private final LayoutInflater mInflater;
        private List<post> mposts;
    private UserViewHolder holder;
    private int position;

    public adapter(Context context) {mInflater=LayoutInflater.from(context); }

        @NonNull
        @Override
        public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemview=mInflater.inflate(R.layout.item,parent,false);
            return new UserViewHolder(itemview);
        }



    @Override
        public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        this.holder = holder;
        this.position = position;
        if (mposts != null) {
                post current = mposts.get(position);
                holder.Title.setText(current.getTitle());
                holder.content.setText(current.getContent());
            } else {
                // Covers the case of data not being ready yet.
                holder.Title.setText("No post");
            }

        }
        void setUser(List<post> words){
            mposts = mposts;
            notifyDataSetChanged();
        }

        public post  getitem( int pos)
        {
            return mposts.get(pos);
        }
        @Override
        public int getItemCount() {
            if (mposts != null)
                return mposts.size();
            else return 0;
        }


        ////////class interne
        class UserViewHolder extends RecyclerView.ViewHolder {
            private final TextView Title;
            private final TextView  content;

            private UserViewHolder(View itemView) {
                super(itemView);
               Title = itemView.findViewById(R.id.textView1);
               content = itemView.findViewById(R.id.textView2);
            }
        }

    }


