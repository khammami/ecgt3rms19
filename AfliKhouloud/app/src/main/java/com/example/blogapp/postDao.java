package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface postDao   {

    @Insert
    public void insert(post p);


    @Delete
    public void delete(post p);

    @Query("delete from post_table")
    public void deleteall();

    @Query("SELECT * from post_table ")
    LiveData<List<post>> getAllposts();





}
