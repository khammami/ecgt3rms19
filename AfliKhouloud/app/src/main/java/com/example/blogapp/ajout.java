package com.example.blogapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

public class ajout extends AppCompatActivity {

    TextView e3;
    EditText e1;
    EditText e2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);
        e1=(EditText)findViewById(R.id.Title);
        e2=(EditText)findViewById(R.id.content);
        e3=(TextView)findViewById(R.id.textView3);

    }

    public void ajouter(View view) {
        String Title=e1.getText().toString();
        String content=e2.getText().toString();
        Date  publeshed_on=e3.getText().toString();
         Intent i = new Intent();
        i.putExtra("Title",Title);
        i.putExtra("DATE",publeshed_on);
        i.putExtra("content",content);
        setResult(RESULT_OK,i);
        finish();




    }
}
