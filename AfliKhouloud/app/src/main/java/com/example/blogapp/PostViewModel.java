package com.example.blogapp;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PostViewModel {


    private POSTRepository mRepository;

    private LiveData<List<post>> mAllposts;

    public PostViewModel (Application application) {

        mRepository = new POSTRepository(application);
        mAllposts = mRepository.getmAllposts();
    }

    LiveData<List<post>> getAllposts() { return mAllposts; }

    public void insert(post post) { mRepository.insert(post); }
    public void delete(post post ){ mRepository.delete(post); }
    public void deleteall() { mRepository.deleteall(); }
}
