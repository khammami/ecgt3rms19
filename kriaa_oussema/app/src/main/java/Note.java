
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "note_table")
public class Note{

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String content;
    private String published_on;

    public Note( int id,String title,String content,String published_on ) {
        this.title = title;
        this.content = content;
        this.published_on =published_on;}

    public String getTitle(){return this.title;}

    public void setId(int id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublished_on(String published_on) {
        this.published_on = published_on;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {return this.content; }
    public String getPublished_on(){return this.published_on;}

    public int getId() { return this.id; }
}