import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteRepository {


    private NoteDao noteDao;
    private LiveData<List<Note>> mAllNote;

    NoteRepository(Application application) {
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        noteDao = db.noteDao();
        mAllNote = noteDao.getAllNotes();
    }

    LiveData<List<Note>> getAllWords() {
        return mAllNote;
    }

    public void insert (Note note) {
        new insertAsyncTask(noteDao).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao mAsyncTaskDao;

        insertAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}