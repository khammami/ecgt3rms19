import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("DELETE FROM note_table")
    void update(Note note);

    @Query("DELETE FROM note_table")
    void deleteAll();

    @Delete
    void deleteWord(Note note);

    @Query("SELECT * from note_table ORDER BY id ASC")
    LiveData<List<Note>> getAllNotes();


}