package com.example.blogapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "Post_table")
public class Post {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private int id ;
    private String title;
    private Date date ;
    private String content ;
    public Post(@NonNull String title) {
        this.title = title;
        this.date = date;
        this.content = content ;
        this.id = id ;
    }


    @NonNull
    public String getTitle() { return title; }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    public String getContent() { return content; }

    public void setContent(String content) { this.content = content; }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}