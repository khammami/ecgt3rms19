package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private final LayoutInflater mInflater;
    private List<Post>mPosts ;
    PostAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        if (mPosts != null) {
            Post current = mPosts.get(position);
            holder.IdView.setText(current.getId());
            holder.TitleView.setText(current.getId());
            holder.ContentView.setText(current.getId());

        }
    }

    void setWords(List<Post> posts){
        mPosts = posts;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mPosts != null)
            return mPosts.size();
        else return 0;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        private final TextView ContentView;
        private final TextView IdView ;
        private final TextView TitleView;

        private PostViewHolder(View itemView) {
            super(itemView);
            IdView = itemView.findViewById(R.id.textView1);
            TitleView = itemView.findViewById(R.id.textView2);
            ContentView = itemView.findViewById(R.id.textView3);

        }
    }
    public Post getPostAtPosition (int position) {
        return mPosts.get(position);
    }
}

