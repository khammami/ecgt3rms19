package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PostDao {

    @Insert
    void insert(Post post);
    @Delete
    void delete (Post post );

    @Query("DELETE FROM Post_table")
    void deleteAll();

    @Query("SELECT * from post_table")
    LiveData<List<Post>> getAllPosts();
    @Query("SELECT * from post_table LIMIT 1")
    Post[] getAnyPost();
}