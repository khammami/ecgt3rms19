package com.example.blogapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PostRepository {
    private PostDao postDao;
    private LiveData<List<post>> mAllPosts;

    PostRepository(Application application) {
        PostRoomDatabase db = PostRoomDatabase.getDatabase(application);
        postDao = db.postDao();
        mAllPosts = postDao.getAllposts();
    }

    LiveData<List<post>> getAllPosts() {
        return mAllPosts;
    }

    public void insert (post post) {
        new insertAsyncTask(postDao).execute(post);
    }

    private static class insertAsyncTask extends AsyncTask<post, Void, Void> {

        private PostDao mAsyncTaskDao;

        insertAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final post... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
