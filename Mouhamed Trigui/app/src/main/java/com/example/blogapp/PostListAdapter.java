package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.blogapp.R;
import com.example.blogapp.post;

import java.util.List;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.postViewHolder> {

    private final LayoutInflater mInflater;
    private List<post> mposts; // Cached copy of posts

    PostListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public postViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new postViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(postViewHolder holder, int position) {
        if (mposts != null) {
            post current = mposts.get(position);
            holder.postItemView.setText(current.getPost());
        } else {
            // Covers the case of data not being ready yet.
            holder.postItemView.setText("No post");
        }
    }

    void setposts(List<post> posts) {
        mposts = posts;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mposts has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mposts != null)
            return mposts.size();
        else return 0;
    }

    class postViewHolder extends RecyclerView.ViewHolder {
        private TextView postItemView;

        private postViewHolder(View itemView) {
            super(itemView);
            postItemView = itemView.findViewById(R.id.textView1);
            postItemView = itemView.findViewById(R.id.textView2);
            postItemView = itemView.findViewById(R.id.textView3);
        }
    }
}