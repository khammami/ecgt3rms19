package com.example.blogapp;

import androidx.annotation.NonNull;


//use androidx instead
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(tableName = "post_table")
public class post {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "post")
    private String post;
    private String date;
    private String content;

    public post(String post , String date , String content){
        this.post=post;
        this.date=date;
        this.content=content;

    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}