package com.example.blogapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PostViewModel extends AndroidViewModel {
    private PostRepository mRepository;

    private LiveData<List<post>> mAllposts;
    public PostViewModel(@NonNull Application application) {
        super(application);
         mRepository = new PostRepository(application);
        LiveData<List<post>> mAllposts = mRepository.getAllPosts();

    }
    LiveData<List<post>> getAllWords() { return mAllposts  ; }

    public void insert(post post) { mRepository.insert(post); }
}
