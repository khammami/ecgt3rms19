package com.example.noteapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListeAdapter extends RecyclerView.Adapter<NoteListeAdapter.noteViewHolder> {

    private final LayoutInflater mInflater;
    private List<note> mnotes; // Cached copy of words

    NoteListeAdapter(Context context) { mInflater = LayoutInflater.from(context); }
    @Override
    public noteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclview_item, parent, false);
        return new noteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(noteViewHolder holder, int position) {
        if (mnotes != null) {
            note current = mnotes.get(position);
            holder.t1.setText(current.getTitle());
            holder.t2.setText(current.getContent());
            holder.t3.setText(current.getPublished_on().toString());
        }
    }

    void setWords(List<note> words){
        mnotes = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mnotes != null)
            return mnotes.size();
        else return 0;
    }

    class noteViewHolder extends RecyclerView.ViewHolder {
        private final TextView t1;
        private final TextView t2;
        private final TextView t3;

            private noteViewHolder(View itemView) {
            super(itemView);
            t1 = itemView.findViewById(R.id.textView2);
            t2 = itemView.findViewById(R.id.textView3);
            t3 = itemView.findViewById(R.id.textView4);

        }
    }
}