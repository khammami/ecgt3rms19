package com.example.blogapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PostRepository {

    private PostDao mPostDao;
    private LiveData<List<Post>> mAllpubs;

   PostRepository(Application application) {
       PostRoomDatabase db = PostRoomDatabase.getDatabase(application);
        mPostDao = db.postDao();
       mAllpubs = mPostDao.getAllPubs();
    }

    LiveData<List<Post>> getAllWords() {
        return mAllpubs;
    }

    public void insert (Post post) {
        new insertAsyncTask(mPostDao).execute(post);
    }

    private static class insertAsyncTask extends AsyncTask<Post, Void, Void> {

        private PostDao mAsyncTaskDao;

        insertAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }}
