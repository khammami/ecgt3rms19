package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.blogapp.Post;

import java.util.List;

@Dao
public interface PostDao {

        @Insert
        void insert(Post post);

        @Query("DELETE FROM post_table")
        void deleteAll();

        @Query("SELECT * from post_table ORDER BY id ASC")
        LiveData<List<Post>> getAllPubs();
    }

