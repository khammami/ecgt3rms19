package com.example.blogapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "post_table")
public class Post {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "title")
    private String mTitle;
    @ColumnInfo(name = "content")
    private String mContent;
    @ColumnInfo(name = "published_on")
    private Date mPublished_on;

    public Post(String mTitle, String mContent, Date mPublished_on) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mContent = mContent;
        this.mPublished_on = mPublished_on;
    }

    @Ignore
    public Post(@NonNull int mId, String mTitle, String mContent, Date mPublished_on) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mContent = mContent;
        this.mPublished_on = mPublished_on;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmContent() {
        return mContent;
    }

    public Date getmPublished_on() {
        return mPublished_on;
    }

    public int getId(){return mId;}

    public void setmId(int mId) {
        this.mId = mId;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }

    public void setmPublished_on(Date mPublished_on) {
        this.mPublished_on = mPublished_on;
    }
}

