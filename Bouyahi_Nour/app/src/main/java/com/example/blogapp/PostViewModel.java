package com.example.blogapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PostViewModel extends AndroidViewModel {
    private PostRepository mRepository;

    private LiveData<List<Post>> mAllPubs;

    public PostViewModel (Application application) {
        super(application);
        mRepository = new PostRepository(application);
        mAllPubs = mRepository.getAllWords();
    }

    LiveData<List<Post>> getAllPubs() { return mAllPubs; }

    public void insert(Post post) { mRepository.insert(post); }
}
