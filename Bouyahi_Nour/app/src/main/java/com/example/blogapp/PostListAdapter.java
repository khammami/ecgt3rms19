package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder>{
    private final LayoutInflater mInflater;
    private List<Post> mPubs; // Cached copy of words

  PostListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        if (mPubs != null) {
            Post current = mPubs.get(position);
            holder.postItemView.setText(current.getmContent());
            holder.postItemView.setText(current.getmTitle());
            holder.postItemView.setText((CharSequence) current.getmPublished_on());
        } else {
            // Covers the case of data not being ready yet.
            holder.postItemView.setText("No pub");
        }
    }

    void setWords(List<Post> pubs){
        mPubs = pubs;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mPubs != null)
            return mPubs.size();
        else return 0;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        private final TextView postItemView;

        private PostViewHolder(View itemView) {
            super(itemView);
            postItemView = itemView.findViewById(R.id.textView);
        }
    }
}

