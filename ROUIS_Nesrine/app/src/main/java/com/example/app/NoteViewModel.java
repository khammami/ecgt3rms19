package com.example.app;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository mRepository;

    private LiveData<List<Note>> mAllNote;

    public NoteViewModel(Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        mAllNote = mRepository.getAllNote();
    }

    LiveData<List<Note>> getAllWords() {
        return mAllNote;
    }

    public void insert(Note note) {
        mRepository.insert(note);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }
}