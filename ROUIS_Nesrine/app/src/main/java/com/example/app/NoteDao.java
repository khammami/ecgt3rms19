package com.example.app;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
    public interface NoteDao {

        @Insert
        void insert(Note note);

        @Query("DELETE FROM note_table")
        void deleteAll();

        @Query("SELECT * from note_table ORDER BY mNote ASC")
        List<Note> getAllNote();

    }

