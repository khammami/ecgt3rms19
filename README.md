# EnetCom GT3 RMS 2019

## NoteApp

* [Master (Same as the practical)](https://github.com/khammami/enetComNoteApp/tree/master) - [download](https://github.com/khammami/enetComNoteApp/archive/master.zip)
* [Alternative 1](https://github.com/khammami/enetComNoteApp/tree/alternative1) - [diff](https://github.com/khammami/enetComNoteApp/compare/master...alternative1) - [download](https://github.com/khammami/enetComNoteApp/archive/alternative1.zip)
* [Alternative 2](https://github.com/khammami/enetComNoteApp/tree/alternative2) - [diff](https://github.com/khammami/enetComNoteApp/compare/alternative1...alternative2) - [download](https://github.com/khammami/enetComNoteApp/archive/alternative2.zip)

## JournalApp

* [Master (Same as the practical)](https://github.com/khammami/enetComJournalApp/tree/master) - [download](https://github.com/khammami/enetComJournalApp/archive/master.zip)
* [Alternative 1](https://github.com/khammami/enetComJournalApp/tree/alternative1) - [diff](https://github.com/khammami/enetComJournalApp/compare/master...alternative1) - [download](https://github.com/khammami/enetComJournalApp/archive/alternative1.zip)
* [Alternative 2](https://github.com/khammami/enetComJournalApp/tree/alternative2) - [diff](https://github.com/khammami/enetComJournalApp/compare/alternative1...alternative2) - [download](https://github.com/khammami/enetComJournalApp/archive/alternative2.zip)

## BlogApp

* [Master (Same as the practical)](https://github.com/khammami/enetComBlogApp/tree/master) - [download](https://github.com/khammami/enetComBlogApp/archive/master.zip)
* [Alternative 1](https://github.com/khammami/enetComBlogApp/tree/alternative1) - [diff](https://github.com/khammami/enetComBlogApp/compare/master...alternative1) - [download](https://github.com/khammami/enetComBlogApp/archive/alternative1.zip)
* [Alternative 2](https://github.com/khammami/enetComBlogApp/tree/alternative2) - [diff](https://github.com/khammami/enetComBlogApp/compare/alternative1...alternative2) - [download](https://github.com/khammami/enetComBlogApp/archive/alternative2.zip)