package com.example.blogapp;
import android.icu.text.CaseMap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "Post_table")
public class Message {

    @PrimaryKey(autoGenerate=true)
    int id;
    String title;
    String content;
    Date published_on;
    private String mMessage;

    public Message(@NonNull String Message) {this.mMessage = Message;}

    public String getMessage(){return this.mMessage;}

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }
}
