package com.example.blogapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Message.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class MessageRoomDatabase extends RoomDatabase {
    public abstract MessageDao MessageDao();
    private static MessageRoomDatabase INSTANCE;

    static MessageRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MessageRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MessageRoomDatabase.class, "Message_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
        @Override
        protected Void doInBackground(final Void... params) {

            // If we have no words, then create the initial list of words
            MessageDao mDao;
            if (mDao.getAnyWord().length < 1) {
                for (int i = 0; i <= Messages.length - 1; i++) {
                    Message Message = new Message(Messages[i]);
                    mDao.insert(Message);
                }
            }
            return null;
        }
    }

}
