package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MessageDao {
    @Insert
    void insert(Message Message);

    @Query("DELETE FROM Post_table")
    void deleteAll();

    @Query("SELECT * from Post_table ORDER BY Message ASC")
    LiveData<List<Message>> getAllMessages();
    @Query("SELECT * from Post_table LIMIT 1")
    Message[] getAnyWord();
}

