package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MessageListAdapter  extends RecyclerView.Adapter<MessageListAdapter.MessageViewHolder> {
    private final LayoutInflater mInflater;
    private List<Message> mMessages; // Cached copy of words

    MessageListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        if (mMessages != null) {
            Message current = mMessages.get(position);
            holder.MessageItemView.setText(current.getMessage());
        } else {
            // Covers the case of data not being ready yet.
            holder.MessageItemView.setText("No Message");
        }
    }

    void setMessages(List<Message> Messages){
        mMessages = Messages;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mMessages has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mMessages != null)
            return mMessages.size();
        else return 0;
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        private final TextView MessageItemView;

        private MessageViewHolder(View itemView) {
            super(itemView);
            MessageItemView = itemView.findViewById(R.id.textView);
        }
    }
}
