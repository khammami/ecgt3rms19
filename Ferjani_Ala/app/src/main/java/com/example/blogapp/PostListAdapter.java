package com.example.blogapp;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder> {
    {
        private final LayoutInflater mInflater;
        private List<Post> mPosts; // Cached copy of words

        PostListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

        @Override
        public PostViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new PostViewHolder(itemView);
    }

        @Override
        public void onBindViewHolder (PostViewHolder holder,int position){
        if (mPosts != null) {
            Post current = mPosts.get(position);
            holder.wordItemView.setText(current.getPost());
        } else {
            // Covers the case of data not being ready yet.
            holder.wordItemView.setText("No Post");
        }
    }

        void setPosts (List < Post > posts) {
        mPosts = posts;
        notifyDataSetChanged();
    }

        // getItemCount() is called many times, and when it is first called,
        // mWords has not been updated (means initially, it's null, and we can't return null).
        @Override
        public int getItemCount () {
        if (mPosts != null)
            return mPosts.size();
        else return 0;
    }
    }
}