package com.example.blogapp;
@Database(entities = {Post.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class PostRoomDataBase extends RoomDatabase  {
    public abstract PostDao wordDao();
    private static PostRoomDataBase INSTANCE;

    static PostRoomDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PostRoomDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PostRoomDataBase.class, "Post_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };
}