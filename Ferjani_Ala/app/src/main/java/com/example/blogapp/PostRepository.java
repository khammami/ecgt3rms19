package com.example.blogapp;

public class PostRepository {
    private PostDao mPostDao;
    private LiveData<List<Post>> mAllWords;

    PostRepository(Application application) {
        PostRoomDataBase db = PostRoomDataBase.getDatabase(application);
        mPostDao = db.wordDao();
        mAllWords = mPostDao.getAllWords();
    }

    LiveData<List<Post>> getAllWords() {
        return mAllWords;
    }

    public void insert (Post post) {
        new insertAsyncTask(mPostDao).execute(post);
    }

    private static class insertAsyncTask extends AsyncTask<id, Void, Void> {

        private PostDao mAsyncTaskDao;

        insertAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
    private static class deleteAllPostsAsyncTask extends AsyncTask<Void, Void, Void> {
        private WordDao mAsyncTaskDao;

        deleteAllPostsAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
    public void deleteAll()  {
        new deleteAllPostsAsyncTask(mPostDao).execute();
    }
    private static class deleteWordAsyncTask extends AsyncTask<Word, Void, Void> {
        private WordDao mAsyncTaskDao;

        deletePostAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post... params) {
            mAsyncTaskDao.deletePost(params[0]);
            return null;
        }
    }
    public void deletePost(Post post)  {
        new deletePostAsyncTask(mPostDao).execute(post);
    }
}
