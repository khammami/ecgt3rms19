package com.example.blogapp;
@Dao
public interface PostDao {
    @Insert
    void insert(Post post);

    @Query("DELETE FROM post_table")
    void deleteAll();
    @Query("SELECT * from post_table LIMIT 1")
    Post[] getAnyPost();
    @Query("SELECT * from post_table ")
    LiveData<List<Post> getAllWords();
    @Query("DELETE FROM post_table")
    void deleteAll();
    @Delete
    void deletePost(Post post);

}