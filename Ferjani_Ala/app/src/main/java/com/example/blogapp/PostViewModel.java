package com.example.blogapp;

public class PostViewModel  extends AndroidViewModel  {
    private PostRepository mRepository;

    private LiveData<List<Post>> mAllWords;

    public PostViewModel (Application application) {
        super(application);
        mRepository = new PostRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<Post>> getAllWords() { return mAllWords; }

    public void insert(Post post) { mRepository.insert(word); }
    public void deleteAll() {mRepository.deleteAll();}
    public void deletePost(Post post) {mRepository.deletePost(post);}
}
