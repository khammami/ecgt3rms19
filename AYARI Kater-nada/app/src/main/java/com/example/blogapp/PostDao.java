package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface PostDao {
    @Insert
    void insert(Post post);

    @Query("DELETE FROM word_table")
    void deleteAll();

    @Query("SELECT * from word_table ORDER BY id ASC")
    LiveData<Post> getAllPost();
}
