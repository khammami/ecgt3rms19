package com.example.blogapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class PostViewModel extends AndroidViewModel {

    private PostRepository mRepository;

    private LiveData<Post> mAllPost;

    public PostViewModel (Application application) {
        super(application);
        mRepository = new PostRepository(application);
        mAllPost = mRepository.getAllPost();
    }

    LiveData<Post> getAllPost() { return mAllPost; }

    public void insert(Post post) { mRepository.insert(post); }

}
