package com.example.blogapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "word_table")

public class Post {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int mid;

    @ColumnInfo(name = "title")
    private String mtitle;

    @ColumnInfo(name = "content")
    private String mcontent;

    @ColumnInfo(name = "published_on")
    private Date mpublished_on;

    public Post(@NonNull int id,String title,String content,Date published_on) {
        this.mid = id;
        this.mtitle = title;
        this.mcontent = content;
        this.mpublished_on= published_on;
    }

    public int getMid() {
        return mid;
    }

    public String getMcontent() {
        return mcontent;
    }

    public String getMtitle() {
        return mtitle;
    }

    public Date getMpublished_on() {
        return mpublished_on;
    }
}
