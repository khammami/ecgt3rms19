package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.PostViewHolder> {
    private final LayoutInflater mInflater;
    private List<Post> posts; // Cached copy of words

    MyAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.rvitem, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        if (posts != null) {
            Post current = posts.get(position);
            holder.title.setText(current.getTitle());
            holder.content.setText(current.getContent());
            holder.date.setText(current.getPublished_on().toGMTString());
        } else {
            holder.title.setText("NoPost");

        }

    }
    public Post getItem(int pos)
    {
        return posts.get(pos);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
    void setPost(List<Post> posts){
        this.posts = posts;
        notifyDataSetChanged();
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private final TextView content;
        private final TextView date;

        private PostViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.textView);
            date = itemView.findViewById(R.id.textView2);
            content = itemView.findViewById(R.id.textView3);
        }
    }
}
