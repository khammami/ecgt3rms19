package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PostDao {

    @Insert
    public void insert (Post p);
    @Delete
    public void delete(Post p);

    @Query("delete from post_table")
    public void deleteall();

    @Query ("select * from post_table")
    public LiveData<List<Post>> getall();
}
