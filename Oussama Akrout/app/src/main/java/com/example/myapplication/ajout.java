package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

public class ajout extends AppCompatActivity {
    EditText e1,e2;
    TextView t;
    Date d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        e1=(EditText)findViewById(R.id.editText);
        e2=(EditText)findViewById(R.id.edittext1);
        t=(TextView)findViewById(R.id.textView4);
        t.setText(new Date().toLocaleString());




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i=new Intent();
                i.putExtra("title",e1.getText().toString());
                i.putExtra("content",e2.getText().toString());
                i.putExtra("date",Converters.dateToTimestamp(d));
                setResult(RESULT_OK, i);
                finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu1, menu);
        return true;
    }
    public void date(View view) {
        DialogFragment newFragment = new datepicker();
        newFragment.show(getSupportFragmentManager(),"datePicker");




    }
    public void processDatePickerResult(Date date) {

        t.setText(date.toLocaleString());
        d=date;

    }

}
