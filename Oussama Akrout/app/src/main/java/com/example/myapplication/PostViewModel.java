package com.example.myapplication;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PostViewModel extends AndroidViewModel {

    private PostRepository mRepository;

    private LiveData<List<Post>> posts;

    public PostViewModel (Application application) {
        super(application);
        mRepository = new PostRepository(application);
        posts = mRepository.getAll();
    }

    LiveData<List<Post>> getAll() { return posts; }

    public void insert(Post p) { mRepository.insert(p);}
    public void delete(Post p) { mRepository.delete(p);}
    public void deleteall() { mRepository.deleteall();}
}