package com.example.myapplication;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Post.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class PostRoom extends RoomDatabase {

    public abstract PostDao postDao();
    private static PostRoom INSTANCE;

    static PostRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PostRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PostRoom.class, "post_database")

                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}

