package com.example.myapplication;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PostRepository {

    private PostDao postDao;
    private LiveData<List<Post>> posts;

    PostRepository(Application application) {
        PostRoom db = PostRoom.getDatabase(application);
        postDao = db.postDao();
        posts=postDao.getall();
    }



    LiveData<List<Post>> getAll() {
        return posts;
    }

    public void insert (Post p) {
        new insertAsyncTask(postDao).execute(p);
    }
    public void delete (Post p) {
        new deleteAsyncTask(postDao).execute(p);
    }
    public void deleteall () {
        new deletealllAsyncTask(postDao).execute();
    }


    private static class insertAsyncTask extends AsyncTask<Post, Void, Void> {

        private PostDao mAsyncTaskDao;

        insertAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
    private static class deleteAsyncTask extends AsyncTask<Post, Void, Void> {

        private PostDao mAsyncTaskDao;

        deleteAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }
    private static class deletealllAsyncTask extends AsyncTask<Void, Void, Void> {

        private PostDao mAsyncTaskDao;

        deletealllAsyncTask(PostDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            mAsyncTaskDao.deleteall();
        return null;
        }
    }
}