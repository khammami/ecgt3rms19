package com.example.blogapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class TableRepository {

 private post_tableDao mtableDao;
        private LiveData<List<Post_table>> mAllWords;

        TableRepository(Application application) {
            TableRoomDatabase db = TableRoomDatabase.getDatabase(application);
            mtableDao = db.post_tableDao();
            mAllWords = mtableDao.getAllWords();
        }

        LiveData<List<Post_table>> getAllWords() {
            return mAllWords;
        }

        public void insert(Post_table table) {
            new insertAsyncTask(mtableDao).execute(table);
        }

        public void deleteAll() {
            new deleteAllWordsAsyncTask(mtableDao).execute();
        }

        // Need to run off main thread
        public void deleteWord(Post_table table) {
            new deleteWordAsyncTask(mtableDao).execute(table);
        }

// Static inner classes below here to run database interactions
// in the background.

/**
 * Insert a word into the database.
 */
private static class insertAsyncTask extends AsyncTask<Post_table, Void, Void> {

    private post_tableDao.WordDao mAsyncTaskDao;

    insertAsyncTask(post_tableDao dao) {
        mAsyncTaskDao = dao;
    }

    protected Void doInBackground(final Post_table... params) {
        mAsyncTaskDao.insert(params[0]);
        return null;
    }
}

/**
 * Delete all words from the database (does not delete the table)
 */
private static class deleteAllWordsAsyncTask extends AsyncTask<Void, Void, Void> {
    private post_tableDao.WordDao mAsyncTaskDao;

    deleteAllWordsAsyncTask(post_tableDao dao) {
        mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        mAsyncTaskDao.deleteAll();
        return null;
    }
}
    private static class deleteAllWordsAsyncTask extends AsyncTask<Void, Void, Void> {
        private post_tableDao mAsyncTaskDao;

        deleteAllWordsAsyncTask(post_tableDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    /**
     *  Delete a single word from the database.
     */
    private static class deleteWordAsyncTask extends AsyncTask<Post_table, Void, Void> {
        private post_tableDao mAsyncTaskDao;

        deleteWordAsyncTask(post_tableDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Post_table... params) {
            mAsyncTaskDao.deleteTable(params[0]);
            return null;
        }
    }
}
