package com.example.blogapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;


@Entity(tableName = "Post_table")
public class Post_table{

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "int")
    private int mid;
    @NonNull
    @ColumnInfo(name = "title")
    private String mtitle;
    @ColumnInfo(name = "date")
    private Date mdate;
    @NonNull
    @ColumnInfo(name = "content")
    private String  mcontent;
    public Post_table(@NonNull int  id, String title , String content , Date date ) {this.mid = id;
        this.mtitle = title;
        this.mcontent = content;
        this.mdate = date;
    }

    public String getPost_table1(){return this.mcontent;}
    public Date getPost_table2(){return this.mdate;}


}