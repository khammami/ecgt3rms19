package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

public interface post_tableDao {


    @Dao
    public interface WordDao {

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        void insert(Post_table Post_table);

        @Query("DELETE FROM Post_table")
        void deleteAll();

        @Delete
        void deleteWord(Post_table ptable);

        @Query("SELECT * from Post_table LIMIT 1")
       Post_table[] getAnyWord();

        @Query("SELECT * from Post_table ORDER BY date ASC")
        LiveData<List<Post_table>> getAllWords();

    }






}
