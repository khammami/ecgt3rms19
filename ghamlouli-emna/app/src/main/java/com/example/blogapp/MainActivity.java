package com.example.blogapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

        private TableViewModel mtableViewModel;

        public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            // Setup the RecyclerView
            RecyclerView recyclerView = findViewById(R.id.recyclerview);
            final TableListAdapter adapter = new TableListAdapter(this);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            // Setup the WordViewModel
            mtableViewModel = ViewModelProviders.of(this).get(TableViewModel.class);
            // Get all the words from the database
            // and associate them to the adapter
            mtableViewModel.getAllWords().observe(this, new Observer<List<Post_table>>() {
                @Override
                public void onChanged(@Nullable final List<Post_table> tables) {
                    // Update the cached copy of the words in the adapter.
                    adapter.setWords(tables);
                }
            });

            // Floating action button setup
            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, NewTableActivity.class);
                    startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
                }
            });

            // Add the functionality to swipe items in the
            // recycler view to delete that item
            ItemTouchHelper helper = new ItemTouchHelper(
                    new ItemTouchHelper.SimpleCallback(0,
                            ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                        @Override
                        // We are not implementing onMove() in this app
                        public boolean onMove(RecyclerView recyclerView,
                                              RecyclerView.ViewHolder viewHolder,
                                              RecyclerView.ViewHolder target) {
                            return false;
                        }

                        @Override
                        // When the use swipes a word,
                        // delete that word from the database.
                        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                            int position = viewHolder.getAdapterPosition();
                            Post_table myTable = adapter.getWordAtPosition(position);
                            Toast.makeText(MainActivity.this,
                                    getString(R.string.delete_word_preamble) + " " +
                                            myTable.getPost_table(), Toast.LENGTH_LONG).show();

                            // Delete the word
                            mtableViewModel.deleteWord(myTable);
                        }
                    });
            // Attach the item touch helper to the recycler view
            helper.attachToRecyclerView(recyclerView);
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        // The options menu has a single item "Clear all data now"
        // that deletes all the entries in the database
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();


            return super.onOptionsItemSelected(item);
        }

        /** When the user enters a new word in the NewWordActivity,
         * that activity returns the result to this activity.
         * If the user entered a new word, save it in the database.

         * @param requestCode -- ID for the request
         * @param resultCode -- indicates success or failure
         * @param data -- The Intent sent back from the NewWordActivity,
         *             which includes the word that the user entered
         */
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
               Post_table table = new Post_table(data.getExtras(NewTableActivity.EXTRA_REPLY));
                // Save the data
                mtableViewModel.insert(table);
            } else {
                Toast.makeText(
                        this, R.string.empty_not_saved, Toast.LENGTH_LONG).show();
            }
        }
    }