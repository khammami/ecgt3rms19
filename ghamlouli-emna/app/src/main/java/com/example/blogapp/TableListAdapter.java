package com.example.blogapp;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;


public class TableListAdapter extends RecyclerView.Adapter<TableListAdapter.TableViewHolder> {

    private final LayoutInflater mInflater;
    private List<Post_table> mTable; // Cached copy of words

    TableListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public TableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new TableViewHolder(itemView);
    }


    public void onBindViewHolder(TableViewHolder holder, int position) {
        if (mTable != null) {
            Post_table current = mTable.get(position);
            holder.TableItemView.setText(current.getPost_table());
        } else {
            // Covers the case of data not being ready yet.
            // holder.wordItemView.setText("No Word");
            holder.TableItemView.setText(R.string.no_word);
        }
    }

    /**
     *     Associate a list of words with this adapter
     */

    void setWords(List<Post_table> table) {
        mTable = table;
        notifyDataSetChanged();
    }



    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).

    public int getItemCount() {
        if (mTable != null)
            return mTable.size();
        else return 0;
    }

    /**
     * Get the word at a given position.
     * This method is useful for identifying which word
     * was clicked or swiped in methods that handle user events.
     *
     * @param position
     * @return The word at the given position
     */
    public Post_table getWordAtPosition(int position) {
        return mTable.get(position);
    }


    public class TableViewHolder extends RecyclerView.ViewHolder {
        private final TextView TableItemView;

        private TableViewHolder(View itemView) {
            super(itemView);
            TableItemView = itemView.findViewById(R.id.textView);
        }
    }}
