package com.example.blogapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class TableViewModel  extends AndroidViewModel {

    private TableRepository mRepository;

    private LiveData<List<Post_table>> mAllWords;

    public TableViewModel(Application application) {
        super(application);
        mRepository = new TableRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<Post_table>> getAllWords() {
        return mAllWords;
    }

    public void insert(Post_table table) {
        mRepository.insert(table);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    public void deleteWord(Post_table table) {
        mRepository.deleteWord(table);
    }
}
