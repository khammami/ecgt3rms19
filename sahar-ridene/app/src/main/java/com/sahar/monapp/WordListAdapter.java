package com.sahar.monapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter {
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {

        private final LayoutInflater mInflater;
        private List<Note> mNotes; // Cached copy of words

        NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

        @Override
        public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
            return new NoteViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(NoteViewHolder holder, int position) {
            if (mNotes != null) {
                Note current = mNotes.get(position);
                holder.noteItemView.setNote(current.getNote());
            } else {
                // Covers the case of data not being ready yet.
                holder.noteItemView.setNote("No note");
            }
        }

        void setNotes(List<Note> Notes){
            mNotes = notes;
            notifyDataSetChanged();
        }

        // getItemCount() is called many times, and when it is first called,
        // mWords has not been updated (means initially, it's null, and we can't return null).
        @Override
        public int getItemCount() {
            if (mNotes != null)
                return mNotes.size();
            else return 0;
        }

        class NoteViewHolder extends RecyclerView.ViewHolder {
            private final TextView noteItemView;

            private NoteViewHolder(View itemView) {
                super(itemView);
                noteItemView = itemView.findViewById(R.id.textView);
            }
        }
    }
}
