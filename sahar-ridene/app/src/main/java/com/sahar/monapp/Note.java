package com.sahar.monapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "note_table")
public class Note {


    @PrimaryKey
    @ColumnInfo(name = "Id")
    private int Id;
    @ColumnInfo(name = "Title")
    private String Title;
    @ColumnInfo(name = "Content")
    private String Content;
    @ColumnInfo(name = "Published")
    private Date Published;

    public Note(int Id, String Title, String Content, Date Published) {
        this.Id = Id;
        this.Title = Title;
        this.Content = Content;
        this.Published = Published;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getPublished() {
        return Published;
    }

    public void setPublished(Date published) {
        Published = published;
    }


}