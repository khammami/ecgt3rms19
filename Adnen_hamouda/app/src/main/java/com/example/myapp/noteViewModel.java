package com.example.myapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class noteViewModel  extends AndroidViewModel {
    private noteRepository mRepository;
    private LiveData<List<note>> mAllnotes;
    public noteViewModel (Application application) {
        super(application);
        mRepository = new noteRepository(application);
        mAllnotes = mRepository.getAllnotes();
    }
    LiveData<List<note>> getAllnotes() { return mAllnotes; }

    public void insert(note note) { mRepository.insert(note); }
}