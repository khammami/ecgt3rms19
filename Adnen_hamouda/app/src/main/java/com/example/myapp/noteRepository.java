package com.example.myapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class noteRepository {

    private noteDao mnoteDao;
    private LiveData<List<note>> mAllnotes;

    noteRepository(Application application) {
        notedatabase db = notedatabase.getDatabase(application);
        mnoteDao = db.noteDao();
        mAllnotes = mnoteDao.getAllnotes();
    }

    LiveData<List<note>> getAllnotes() {
        return mAllnotes;
    }

    public void insert (note note) {
        new insertAsyncTask(mnoteDao).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask<note, Void, Void> {

        private noteDao mAsyncTaskDao;

        insertAsyncTask(noteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final note... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
