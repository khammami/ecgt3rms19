package com.example.myapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class noteListAdapter extends RecyclerView.Adapter<noteListAdapter.noteViewHolder> {

    private final LayoutInflater mInflater;
    private List<note> mnotes; // Cached copy of words

    noteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public noteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new noteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(noteViewHolder holder, int position) {
        if (mnotes != null) {
            note current = mnotes.get(position);
            holder.noteItemView.setText(current.getId());
        } else {
            // Covers the case of data not being ready yet.
            holder.noteItemView.setText("No note");
        }
    }

    void setnotess(List<note> notes){
        mnotes = notes;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mnotes != null)
            return mnotes.size();
        else return 0;
    }

    class noteViewHolder extends RecyclerView.ViewHolder {
        private final TextView noteItemView;

        private noteViewHolder(View itemView) {
            super(itemView);
            noteItemView = itemView.findViewById(R.id.textView);
        }
    }
}
