package com.example.myapp;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {note.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class notedatabase extends RoomDatabase {
    public abstract noteDao noteDao();
    private static notedatabase INSTANCE;
    static notedatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (notedatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            notedatabase.class, "note_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();

                }
            }
        }
        return INSTANCE;
    }



    }

}

