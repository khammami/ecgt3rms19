package com.example.myexam;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository NRepository;

    private LiveData<List<Note>> mAllNotes;

    public NoteViewModel (Application application) {
        super(application);
        NRepository = new NoteRepository(application);
        mAllNotes = NRepository.getAllWords();
    }

    LiveData<List<Note>> getAllWords() { return mAllNotes; }

    public void insert(Note word) { NRepository.insert(word); }
}
