package com.example.myexam;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "table")
public class Note {




    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;

    private String content;
    private Date published_on;
    public Note( String note) {this.title = note;}

    public String getNote(){return this.content;}

    public Note(String title, String content, Date published_on) {
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }
}
