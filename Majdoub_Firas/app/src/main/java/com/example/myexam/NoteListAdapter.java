package com.example.myexam;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {

    private final LayoutInflater mInflater;
    private List<Note> notes; // Cached copy of words

    NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.addnote, parent, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (notes != null) {
            Note current = notes.get(position);
            holder.wordItemView.setText(current.getContent());
        } else {
            // Covers the case of data not being ready yet.
            holder.wordItemView.setText("No Notes added");
        }
    }

    void setWords(List<Note> words){
        notes = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (notes != null)
            return notes.size();
        else return 0;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private final TextView wordItemView;

        private NoteViewHolder(View itemView) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.pubdate);
        }
    }
}
