package com.example.myexam;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("DELETE FROM `table`")
    void deleteAll();

    @Query("SELECT * from `table` ORDER BY published_on ASC")
    LiveData<List<Note>> getAllNotes();
}
