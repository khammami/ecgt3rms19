package com.example.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private final LayoutInflater mInflater;
    private List<table> mtable; // Cached copy of words

    PostAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        if (mtable != null) {
            table current = mtable.get(position);
            holder.postItemView.setText(current.getId());
        } else {
            // Covers the case of data not being ready yet.
            holder.postItemView.setText("No post");
        }
    }

    void setPost(List<table> words){
        mtable = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mtable != null)
            return mtable.size();
        else return 0;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        private final TextView postItemView;

        private PostViewHolder(View itemView) {
            super(itemView);
            postItemView = itemView.findViewById(R.id.textView);
        }
    }

}
