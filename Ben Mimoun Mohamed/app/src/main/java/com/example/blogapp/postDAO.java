package com.example.blogapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface postDAO {


    @Insert
    void insert(table table);

    @Query("DELETE FROM post_table")
    void deleteAll();
    @Delete
    void deleteWord(table table);
    @Query("SELECT * from post_table ORDER BY table ASC")
    LiveData<List<table>> getAllPost();
    @Query("SELECT * from post_table LIMIT 1")
    table[] getAnyPost();
}
