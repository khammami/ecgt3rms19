package com.example.blogapp;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity (tableName = "post_table")
public class table {
    @PrimaryKey(autoGenerate = true)
    @NonNull
  String id;
    String title;
    String content;
    Date published_on;
    public table(@NonNull String word) {this.id = id;}



    public table(String id, String title, String content, Date published_on) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }
}
