package com.example.blogapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class PostViewModel extends AndroidViewModel {

    //public void deleteAll() {mRepository.deleteAll();}
    private PostRepository mRepository;

    private LiveData<List<table>> AllPost;

    public PostViewModel (Application application) {
        super(application);
        mRepository = new PostRepository(application);
        AllPost = mRepository.getAllPost();
    }
    LiveData<List<table>> getAllPost() { return AllPost; }

    public void insert(table table) { mRepository.insert(table); }
}
