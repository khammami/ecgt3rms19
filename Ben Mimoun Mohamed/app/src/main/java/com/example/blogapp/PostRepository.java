package com.example.blogapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PostRepository {
    private postDAO mtable;
    private LiveData<List<table>> AllPost;

   PostRepository(Application application) {
       postRoomDataBase db = postRoomDataBase.getDatabase(application);
        mtable = db.postDAO();
        AllPost = mtable.getAllPost();
    }

    LiveData<List<table>> getAllPost() {
        return AllPost;
    }

    public void insert (table table) {
        new insertAsyncTask(mtable).execute(table);
    }

    private static class insertAsyncTask extends AsyncTask<table, Void, Void> {

        private postDAO mAsyncTaskDao;

        insertAsyncTask(postDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final table... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }

        private static class deletePostAsyncTask extends AsyncTask<table, Void, Void> {
            private postDAO mAsyncTaskDao;

            deletePostAsyncTask(postDAO dao) {
                mAsyncTaskDao = dao;
            }

            @Override
            protected Void doInBackground(final table... params) {
                mAsyncTaskDao.deleteWord(params[0]);
                return null;
            }
        }

        private static class deleteAllPostAsyncTask extends AsyncTask<Void, Void, Void> {
            private postDAO mAsyncTaskDao;

            deleteAllPostAsyncTask(postDAO dao) {
                mAsyncTaskDao = dao;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                mAsyncTaskDao.deleteAll();
                return null;
            }
        }
        //   public void deleteAll()  {
        //     new deleteAllPostAsyncTask(mtable).execute();
        /// }
    }}


