package com.example.blogapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


@Database(entities = {table.class}, version = 1, exportSchema = false)
@TypeConverters({Converts.class})
public abstract class  postRoomDataBase extends RoomDatabase {

    public abstract postDAO postDAO();
    private static postRoomDataBase INSTANCE;

    static postRoomDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (postRoomDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                           postRoomDataBase.class, "post_table")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
