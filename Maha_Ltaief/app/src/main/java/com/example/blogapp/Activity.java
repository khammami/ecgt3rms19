package com.example.blogapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;


public class Activity extends AppCompatActivity {


    public static final String EXTRA_REPLY = "com.example.android.roomwordssample.REPLY";

    private EditText mEditWordView;
    private  EditText mEditWordView2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_);
        mEditWordView = findViewById(R.id.editText);
        mEditWordView2 = findViewById(R.id.editText2);
        Button button1=(Button)findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDatePicker(v);
            }});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent replyIntent = new Intent();
            if (TextUtils.isEmpty(mEditWordView.getText())||TextUtils.isEmpty(mEditWordView2.getText())) {
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                String word = mEditWordView.getText().toString();
                String word2 = mEditWordView2.getText().toString();
                replyIntent.putExtra(EXTRA_REPLY, word);
                replyIntent.putExtra("2", word2);
                replyIntent.putExtra("3", );


                setResult(RESULT_OK, replyIntent);
            }
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    public Date processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        String dateMessage = (month_string +
                "/" + day_string +
                "/" + year_string);
        Toast.makeText(getApplicationContext(), "date" + dateMessage,
                Toast.LENGTH_SHORT).show();
return date=

    }
    public void showDatePicker(View view) {
        DialogFragment newFragment = new BlankFragment2();
        newFragment.show(getSupportFragmentManager(),"hi there");
        //    getString("hi there"));
    }

}
