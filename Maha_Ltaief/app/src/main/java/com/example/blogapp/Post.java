package com.example.blogapp;



import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;


@Entity(tableName = "post_table")
    public class Post {

        @PrimaryKey(autoGenerate = true)
        @NonNull
        @ColumnInfo(name = "id")
        private int id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ColumnInfo(name = "word")
        private String mWord;

@Ignore
    public Post(int id, String mWord, Date mdate, String mcontent) {
        this.id = id;
        this.mWord = mWord;
        this.mdate = mdate;
        this.mcontent = mcontent;
    }

    @ColumnInfo(name = "published_on")
    private Date mdate;

    public void setmWord(String mWord) {
        this.mWord = mWord;
    }

    @ColumnInfo(name = "content")
    private String mcontent;

        public Post() {
           }

    public void setMdate(Date mdate) {
        this.mdate = mdate;
    }

    public void setWord(@NonNull String mWord) {
        this.mWord = mWord;
    }

    public void setMcontent(String mcontent) {
        this.mcontent = mcontent;
    }


    public String getMcontent(){return this.mcontent;}

    public String getWord() {
        return this.mWord;
    }

    public Date getMdate(){return this.mdate;
            }

}