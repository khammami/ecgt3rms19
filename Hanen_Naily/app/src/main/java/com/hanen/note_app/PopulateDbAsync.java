package com.hanen.note_app;

import android.os.AsyncTask;

/**
 * Populate the database in the background.
 */
private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final NoteDao mDao;
    String[] notes = { "title","date", "content"};

    PopulateDbAsync(NoteRoomDatabase db) {
        mDao = db.noteDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        // Start the app with a clean database every time.
        // Not needed if you only populate the database
        // when it is first created
        mDao.deleteAll();

        for (int i = 0; i <= notes.length - 1; i++) {
            Notes note = new Note(notes[i]);
            mDao.insert(note);
        }
        return null;
    }
}