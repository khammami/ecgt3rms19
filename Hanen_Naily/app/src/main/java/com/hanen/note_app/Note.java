package com.hanen.note_app;
//deprecated
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//use androidx instead

@Entity(tableName = "note_table")
public class Note<date> {
    public String mNote;
    @PrimaryKey

    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "title")
        private String title;
    @ColumnInfo(name = "content")
        private String content;

    @ColumnInfo(name = "published_on")
     private date published_on;



    public void setNote(String mNote) {
        this.mNote = mNote;
    }

    public String getNote(String mNote) {
        return this.mNote;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId(int id) {
       return  this.id ;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public String gettitle(String title) {
        return  this.title  ;
    }

    public void setcontent(String content) {
        this.content = content;
    }

    public String gettcontent(String content) {
        return  this.content  ;
    }


    public void setcontent(date published_on) {
        this.published_on = published_on;
    }

    public date gettcontent(date published_on) {
        return  this.published_on ;}

}
