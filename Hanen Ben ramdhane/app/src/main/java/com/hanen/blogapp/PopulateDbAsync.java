package com.hanen.blogapp;

import android.os.AsyncTask;

private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
    private final PostDao mDao;
    String[] posts = {"dolphin", "crocodile", "cobra"};

    PopulateDbAsync(PostRoomDatabase db) {
        mDao = db.postDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        // Start the app with a clean database every time.
        // Not needed if you only populate the database
        // when it is first created
        mDao.deleteAll();

        for (int i = 0; i <= posts.length - 1; i++) {
            Post post = new Post(posts[i]);
            mDao.insert(post);
        }
        return null;
    }
}