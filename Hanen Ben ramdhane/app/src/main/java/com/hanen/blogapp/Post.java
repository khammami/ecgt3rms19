package com.hanen.blogapp;//use androidx instead
import androidx.room.ColumnInfo;
        import androidx.room.Entity;
        import androidx.room.PrimaryKey;
        import androidx.annotation.NonNull;
@Entity(tableName = "post_table")
public class Post {

    @PrimaryKey(autoGenerate = true)
    int id;
    @NonNull
    @ColumnInfo(name = "post")
    private String mPost;

    public Post(@NonNull String post) {this.mPost = post;}

    public String getPost(){return this.mPost;}
}