package com.hanen.blogapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView.Adapter;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder> {

    private final LayoutInflater mInflater;
    private List<Post> mPosts; // Cached copy of posts

    PostListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        if (mPosts != null) {
            Post current = mPosts.get(position);
            holder.postItemView.setText(current.getPost());
        } else {
            // Covers the case of data not being ready yet.
            holder.postItemView.setText("No Post");
        }
    }

    void setPosts(List<Post> posts){
        mPosts = posts;
        notifyDataSetChanged ();
    }

    // getItemCount() is called many times, and when it is first called,
    // mPosts has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mPosts != null)
            return mPosts.size();
        else return 0;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        private final TextView postItemView;

        private PostViewHolder(View itemView) {
            super(itemView);
            postItemView = itemView.findViewById(R.id.textView);
        }
    }
}